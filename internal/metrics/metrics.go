package metrics

import (
	"fmt"
	"time"

	"github.com/christophberger/grada"
	"gitlab.com/Alvoras/balcon/internal/exporters"
	"gitlab.com/Alvoras/balcon/internal/logger"
)

type Metrics struct {
	List []*Metric
}

type Metric struct {
	Name         string `json:"name"`       // Display name
	QueryName    string `json:"query-name"` // URL parameter of the queried exporter
	Exporter     *exporters.Exporter
	ExporterName string `json:"exporter-name"`
	TimeRange    int    `json:"timerange"`
	Interval     int    `json:"interval"`
	Delay        int    `json:"delay"`
	StreamFunc   func() float64
	Buffer       *grada.Metric
}

var (
	MetricsList Metrics
)

func (m *Metrics) Get(name string) (*Metric, error) {
	for _, source := range m.List {
		if source.Name == name {
			return source, nil
		}
	}

	return nil, fmt.Errorf("exporters not found")
}

func (metrics *Metrics) Register(m *Metric, dash *grada.Dashboard) error {
	var err error
	if m.Name == "" {
		m.Name = fmt.Sprintf("%s.%s", m.ExporterName, m.QueryName)
	}

	m.StreamFunc = m.Exporter.GetServiceMetricStream(m.QueryName)
	if m.Buffer, err = dash.CreateMetric(m.Name, time.Duration(m.TimeRange)*time.Minute, time.Duration(m.Interval)*time.Second); err != nil {
		return err
	}

	metrics.List = append(metrics.List, m)
	return nil
}

func (metrics *Metrics) RegisterAll(metricList []*Metric, dash *grada.Dashboard) error {
	for _, metric := range metricList {
		if err := metrics.Register(metric, dash); err != nil {
			logger.Error.Println("Failed to register", metric.Name, err)
		}
	}

	return nil
}

func (metrics *Metrics) RegisterFromConfig(metricList []*Metric, dash *grada.Dashboard) error {
	for _, metric := range metricList {
		if metric.Name == "" {
			metric.Name = fmt.Sprintf("%s.%s", metric.ExporterName, metric.QueryName)
		}

		exporter, err := exporters.ExportersList.Get(metric.ExporterName)
		if err != nil {
			logger.Error.Println(fmt.Sprintf("Failed to register metric %s from the %s exporter : %s", metric.Name, metric.ExporterName, err))
			continue
		}

		metric.Exporter = exporter
		if err := metrics.Register(metric, dash); err != nil {
			logger.Error.Println("Failed to register", metric.Name, err)
		}
	}

	return nil
}

func (metrics *Metrics) PollAll() {
	for _, metric := range metrics.List {
		go metric.Poll()
	}
}

func (m *Metric) Poll() {
	logger.Info.Println(fmt.Sprintf("Started to poll the %s metric at the %s exporters (<%s>)", m.Name, m.Exporter.Name, m.Exporter.Endpoint))

	for {
		data := m.StreamFunc()
		m.Buffer.Add(data)

		time.Sleep(time.Duration(m.Interval) * time.Second)
	}
}
