package exporters

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"gitlab.com/Alvoras/balcon/internal/logger"
)

type Exporters struct {
	List []*Exporter `json:"exporters"`
}

type Auth struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type Exporter struct {
	Name     string `json:"name"`
	Endpoint string `json:"endpoint"`
	Auth     *Auth  `json:"auth"`
}

var (
	ExportersList Exporters
)

func (exporters *Exporters) Get(name string) (*Exporter, error) {
	for _, source := range exporters.List {
		if source.Name == name {
			return source, nil
		}
	}

	return nil, fmt.Errorf("exporter not found")
}

func (exporters *Exporters) Register(name string, endpoint string, auth *Auth) error {
	// _, _, err := fasthttp.Get(nil, endpoint)
	// if err != nil {
	// 	return err
	// }

	source := &Exporter{
		Name:     name,
		Endpoint: endpoint,
		Auth:     auth,
	}

	client := &http.Client{}
	req, err := http.NewRequest("GET", source.Endpoint, nil)

	if source.Auth != nil {
		req.SetBasicAuth(source.Auth.Username, source.Auth.Password)
	}

	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		return fmt.Errorf(fmt.Sprintf("error checking endpoint %s : got status %d", source.Endpoint, resp.StatusCode))
	}

	exporters.List = append(exporters.List, source)
	return nil
}

func (exporters *Exporters) RegisterAll(sources []Exporter) error {
	for _, source := range sources {
		if err := exporters.Register(source.Name, source.Endpoint, source.Auth); err != nil {
			logger.Error.Println("Failed to register", source.Name, err)
		}
	}

	return nil
}
func (exporters *Exporters) RegisterFromConfig(sources []*Exporter) error {
	for _, source := range sources {
		if err := exporters.Register(source.Name, source.Endpoint, source.Auth); err != nil {
			logger.Error.Println(fmt.Sprintf("Failed to register the %s exporter at %s", source.Name, source.Endpoint))
		}
	}

	return nil
}

func (exporter *Exporter) QueryService(queryName string) float64 {
	client := &http.Client{}
	req, err := http.NewRequest("GET", fmt.Sprintf("%s/query/%s", exporter.Endpoint, queryName), nil)

	if exporter.Auth != nil {
		req.SetBasicAuth(exporter.Auth.Username, exporter.Auth.Password)
	}

	resp, err := client.Do(req)
	if err != nil {
		return 0
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return 0
	}

	body, err := ioutil.ReadAll(resp.Body)

	f64Body, err := strconv.ParseFloat(string(body[:]), 64)
	if err != nil {
		return 0
	}

	return f64Body
}

func (exporter *Exporter) GetServiceMetricStream(queryName string) func() float64 {
	return func() float64 {
		return exporter.QueryService(queryName)
	}
}
