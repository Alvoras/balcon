package config

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"gitlab.com/Alvoras/balcon/internal/exporters"
	"gitlab.com/Alvoras/balcon/internal/logger"
	"gitlab.com/Alvoras/balcon/internal/metrics"
)

var (
	Cfg = new(Config)
)

// Config structure which mirrors the json file
type Config struct {
	Exporters []*exporters.Exporter `json:"exporters"`
	Metrics   []*metrics.Metric     `json:"metrics"`
	Port      string                `json:"port"`
}

func (cfg *Config) Load() {
	filepath := "config.json"
	cfgData, err := ioutil.ReadFile(filepath)
	if err != nil {
		logger.Error.Println("Failed to load config file", err)
	}

	if err := json.Unmarshal(cfgData, &cfg); err != nil {
		logger.Error.Printf("Failed to load the config file (%s)\n", filepath)
		logger.Error.Println(err)
		os.Exit(1)
	}
}
