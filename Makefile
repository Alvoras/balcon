default:
	echo "No default make"

install:
	go build -o balcon
	mkdir /opt/balcon
	cp ./balcon /opt/balcon/
	cp ./config.sample.json /opt/balcon/config.json
	echo "Now update /opt/balcon/config.json"

startup:
	sudo ln -rs balcon.service /etc/systemd/system/balcon.service
	sudo systemctl enable balcon
