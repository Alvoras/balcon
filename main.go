package main

import (
	"github.com/christophberger/grada"
	"gitlab.com/Alvoras/balcon/internal/config"
	"gitlab.com/Alvoras/balcon/internal/exporters"
	"gitlab.com/Alvoras/balcon/internal/logger"
	"gitlab.com/Alvoras/balcon/internal/metrics"
)

func init() {
	config.Cfg.Load()
}

func main() {
	logger.Info.Println("Starting server at port", config.Cfg.Port)
	dash := grada.GetDashboard(config.Cfg.Port)

	exporters.ExportersList.RegisterFromConfig(config.Cfg.Exporters)
	metrics.MetricsList.RegisterFromConfig(config.Cfg.Metrics, dash)

	go metrics.MetricsList.PollAll()

	select {}
}
